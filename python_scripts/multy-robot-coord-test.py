# -*- coding: utf-8 -*-
# !/usr/bin/python

import threading
import time
import struct
import serial


INIT = "55"
DEBUG = "1111"
END = "5555"
# MOVE_FORWARD = "1001"
MESSAGE_INIT_LEN = 2
MESSAGE_TYPE_LEN = 4
MESSAGE_LENGTH_LEN = 4
byte_type = "c"
byte_amount_in_type = 1


def check_number(check_str):
    ans = True
    for check_str_itt in check_str:

        if check_str_itt == "-" or check_str_itt == "+" or check_str_itt == "." or ("0" <= check_str_itt <= "9"):
            continue
        else:
            ans = False
            break
    return ans


def read_n_chars(n, robot):
    """ should return char string """
    reply = ""
    while len(reply) != n:
        reply += robot.read()
    #     need to test reply = robot.read(n)
    # print("unpack int: ", struct.unpack("@" + "i"*len(reply), reply))
    return reply


def read_messages(robot):
    """ return message string """
    reply = byte_string_to_number(read_n_chars(MESSAGE_LENGTH_LEN, robot))
    message = read_n_chars(reply, robot)

    return message


def debug_robot(robot, id):
    reply = read_messages(robot)
    print("DEBUG ROBOT ", id, " :", reply)


def debug_coord(robot):
    length = byte_string_to_number(read_n_chars(MESSAGE_LENGTH_LEN, robot))

    x = ""
    while len(x) < length / 2:
        x += robot.read()

    y = ""
    while len(y) < length / 2:
        y += robot.read()

    print("DEBUG x, y:", x, y)


def byte_string_to_number(byte_reply):
    ans = 0
    for i in byte_reply:
        ans *= 10
        ans += int(i)
    return ans


def write_to_file(robot_id, message, lock):
    lock.acquire()
    file = open('coordinates.txt', 'a')
    file.write(robot_id+' '+message+'\n')
    file.close()
    lock.release()



class MyThread(threading.Thread):
    def __init__(self, thread_id, robots):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.robots = robots
        self.reply = ""
        self.message = ""

    def run(self):
        if len(robots) > 1:
            run_thread(self, robots, self.thread_id, self.reply, self.message)
        else:
            run_single_thread(robots[self.thread_id])


def run_thread(thread, robots, robot_id, reply, message):
    loop = True
    lock = threading.Lock()

    # lock.acquire()
    # lock.wait()

    print('Введите X для робота {} в формате : #### (см)'.format(robot_id))

    if robot_id == 0:
        message1 = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '0'.encode('ascii'),
                              'L'.encode('ascii'), 'S'.encode('ascii'))
        robots[0].write(message1)

        time.sleep(1)

        message2 = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '1'.encode('ascii'),
                               'F'.encode('ascii'), 'I'.encode('ascii'))
        robots[0].write(message2)

    else:
        message1 = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '0'.encode('ascii'),
                               'L'.encode('ascii'), 'I'.encode('ascii'))
        robots[1].write(message1)

        time.sleep(1)

        message2 = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '1'.encode('ascii'),
                               'F'.encode('ascii'), 'S'.encode('ascii'))
        robots[1].write(message2)


    time.sleep(1)

    # timer = time.time()


    if robot_id == 0:
        x = "0004"
    else:
        x = "0020"

    if robot_id == 0:
        y = "0020"
    else:
        y = "0020"

    print('Введите Y для робота {} в формате : #### (см)'.format(robot_id))
    print('robot_id={}, x y = {} {}'.format(robot_id, x, y))

    # lock.notify()
    # lock.release()
    if robot_id == 0:
        message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
                              '0040'.encode('ascii'))
        robots[0].write(message)
        # time.sleep(1)
        #
        # message3 = struct.pack("<s" + "s" + "4s" + "4s", 'm'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
        #                       '0060'.encode('ascii'))
        # robots[0].write(message3)
    else:
        message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
                              '0020'.encode('ascii'))
        robots[1].write(message)

    time.sleep(1)

    # sent = 1

    # if "x" not in message:
    #     print("{} message start".format(robot_id), message)
    # robots[robot_id].write(message)

    if robot_id == 0:
        print ('wait')
        time.sleep(1)
        message = struct.pack("<s" + "s" + "4s" + "4s", 'm'.encode('ascii'), '8'.encode('ascii'), '0020'.encode('ascii'),
                          '0060'.encode('ascii'))
        robots[0].write(message)
        print ('sent--------------------')

    while loop:
        try:
            reply = robots[robot_id].read()
        except Exception:
            print("can't read from robot {}".format(robot_id))
            loop = False

            # if robot_id == 1:
            #     if time.time() - timer > 10 and sent == 1:
            #         message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
            #                               '0060'.encode('ascii'))
            #         robots[0].write(message)
            #         sent = 0
            #         print("-----------------------Send next message ----------------------")


        if reply == "d":
            num = read_n_chars(2, robots[robot_id])

            print("{} num: ".format(robot_id), num)

            reply = read_n_chars(int(num), robots[robot_id])

            write_to_file(robot_id, reply, lock)

            print("{} end reply: ".format(robot_id), reply)

        elif reply == "n":
            size = read_n_chars(1, robots[robot_id])
            print("{} size".format(robot_id), size)

            coordinates = read_n_chars(int(size), robots[robot_id])
            print(coordinates)

            if check_number(coordinates):

                message = struct.pack("<s" + "s" + size + "s", 'n'.encode('ascii'), size.encode('ascii'), coordinates)
                print("{} message with n".format(robot_id), message)

                for robot_itt in range(len(robots)):
                    if robot_itt != robot_id:
                        if "x" not in message:
                            robots[robot_itt].write(message)
                            print("send to robot {} mes:".format(robot_itt), message)

        reply = ""


def run_single_thread(robot):
    reply = ""

    print('Введите X в формате: #### (см)')
    x = raw_input()

    print('Введите Y в формате: #### (см)')
    y = raw_input()

    message = struct.pack("<s" + "s" + "4s" + "4s", 'm'.encode('ascii'), '8'.encode('ascii'), str(x).encode('ascii'),
                          str(y).encode('ascii'))

    robot.write(message)

    reply = ""
    itt = 0
    timer = time.time()

    reply = ""
    while True:

        if time.time() - timer > 2 and itt > 0:
            robot.write("f")
            itt = 0

        reply = robot.read()
        if reply == "d":
            itt += 1
            timer = time.time()

            reply = robot.read()

            while len(reply) < 2:
                reply += robot.read()

            num = reply
            reply = ""
            print("num: ", num)
            for i in range(int(num)):
                reply += robot.read()

            print("end reply: ", reply)

            if reply == "add_mes":
                print("reply == 'add_mes'")
                m = struct.pack("s", "f")
                robot.write("f")


robot_numbers = 2
threads = []
robots = []
# serial_ports_array = ['/dev/cu.e-puck2_04217-UART', '/dev/cu.e-puck2_04213-UART']
serial_ports_array = ['/dev/cu.e-puck2_04213-UART', '/dev/cu.e-puck2_04217-UART']
error_loaded = []

for i in range(robot_numbers):
    try:
        robots.append(serial.Serial(serial_ports_array[i], 115200,
                                    timeout=0))  # Specify the robot communication port (after pairing).
    except Exception:
        error_loaded.append(i)
        print("cannot connect to robot i:", i)

if len(error_loaded) == 0:
    for i in range(robot_numbers):
        threads.append(MyThread(i, robots))

    for i in range(robot_numbers):
        threads[i].start()

    for i in range(robot_numbers):
        threads[i].join()

for i in range(robot_numbers):
    if not (i in error_loaded):
        robots[i].close()

print "Exiting Main Thread"
