# -*- coding: utf-8 -*-
# !/usr/bin/python

import threading
import time
import struct
import serial


INIT = "55"
DEBUG = "1111"
END = "5555"
# MOVE_FORWARD = "1001"
MESSAGE_INIT_LEN = 2
MESSAGE_TYPE_LEN = 4
MESSAGE_LENGTH_LEN = 4
byte_type = "c"
byte_amount_in_type = 1

WRITE_FILE = 1


def check_number(check_str):
    ans = True
    for check_str_itt in check_str:

        if check_str_itt == "-" or check_str_itt == "+" or check_str_itt == "." or ("0" <= check_str_itt <= "9"):
            continue
        else:
            ans = False
            break
    return ans


def read_n_chars(n, robot):
    """ should return char string """
    reply = ""
    while len(reply) != n:
        reply += robot.read()
    #     need to test reply = robot.read(n)
    # print("unpack int: ", struct.unpack("@" + "i"*len(reply), reply))
    return reply


def read_messages(robot):
    """ return message string """
    reply = byte_string_to_number(read_n_chars(MESSAGE_LENGTH_LEN, robot))
    message = read_n_chars(reply, robot)

    return message


def debug_robot(robot, id):
    reply = read_messages(robot)
    print("DEBUG ROBOT ", id, " :", reply)


def debug_coord(robot):
    length = byte_string_to_number(read_n_chars(MESSAGE_LENGTH_LEN, robot))

    x = ""
    while len(x) < length / 2:
        x += robot.read()

    y = ""
    while len(y) < length / 2:
        y += robot.read()

    print("DEBUG x, y:", x, y)


def byte_string_to_number(byte_reply):
    ans = 0
    for i in byte_reply:
        ans *= 10
        ans += int(i)
    return ans


def write_to_file(robot_id, message, lock):
    if WRITE_FILE == 1:
        lock.acquire()
        file = open('nothing_just_fun_4.txt', 'a')
        file.write(str(robot_id)+' '+str(message)+' '+ str(time.clock()) +'\n')
        file.close()
        lock.release()



class MyThread(threading.Thread):
    def __init__(self, thread_id, robots):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.robots = robots
        self.reply = ""
        self.message = ""

    def run(self):
        if len(robots) > 1:
            run_thread(self, robots, self.thread_id, self.reply, self.message)
        else:
            run_single_thread(robots[self.thread_id])


def run_thread(thread, robots, robot_id, reply, message):
    loop = True
    # lock = threading.Condition()
    lock = threading.Lock()

    # rel = {0:[1,2],1:[3],2:[4]}
    # rel = {0:[1,2,3,4]}
    rel = {0:[1,2]}

    # lock.acquire()
    # lock.wait()

    print('Введите X для робота {} в формате : #### (см)'.format(robot_id))

    # if robot_id == 0:
    #     message1 = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '0'.encode('ascii'),
    #                           'L'.encode('ascii'), 'S'.encode('ascii'))
    #     robots[0].write(message1)
    #
    #     time.sleep(1)
    #
    #     message2 = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '1'.encode('ascii'),
    #                            'F'.encode('ascii'), 'I'.encode('ascii'))
    #     robots[0].write(message2)
    #
    # else:
    #     message1 = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '0'.encode('ascii'),
    #                            'L'.encode('ascii'), 'I'.encode('ascii'))
    #     robots[robot_id].write(message1)
    #
    #     time.sleep(robot_id)
    #
    #     message2 = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '1'.encode('ascii'),
    #                            'F'.encode('ascii'), 'S'.encode('ascii'))
    #     robots[robot_id].write(message2)

    # for i in range(len(robots)):
    #     if robot_id == 0:
    #         if i == 0:
    #             mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '0'.encode('ascii'),
    #                                 'L'.encode('ascii'), 'S'.encode('ascii'))
    #             time.sleep(1)
    #         else:
    #             mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), str(i).encode('ascii'),
    #                               'F'.encode('ascii'), 'I'.encode('ascii'))
    #             time.sleep(1)
    #
    #     else:
    #         if i == 0:
    #             mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '0'.encode('ascii'),
    #                               'L'.encode('ascii'), 'I'.encode('ascii'))
    #             time.sleep(1)
    #         elif i == robot_id:
    #             mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), str(i).encode('ascii'),
    #                               'F'.encode('ascii'), 'S'.encode('ascii'))
    #             time.sleep(1)
    #         else:
    #             mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), str(i).encode('ascii'),
    #                               'F'.encode('ascii'), 'I'.encode('ascii'))
    #             time.sleep(1)
    #
    #     robots[robot_id].write(mes)

    # todo Топология ЗВЕЗДА
    for i in range(len(robots)):
        if robot_id == 0:
            if i == 0:
                mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '0'.encode('ascii'),
                                  'L'.encode('ascii'), 'S'.encode('ascii'))
                time.sleep(1)
            else:
                mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), str(i).encode('ascii'),
                                  'F'.encode('ascii'), 'I'.encode('ascii'))
                time.sleep(1)

        else:
            if i == 0:
                mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '0'.encode('ascii'),
                                  'L'.encode('ascii'), 'I'.encode('ascii'))
                time.sleep(1)
            elif i == robot_id:
                mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), str(i).encode('ascii'),
                                  'F'.encode('ascii'), 'S'.encode('ascii'))
                time.sleep(1)
            else:
                mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), str(i).encode('ascii'),
                                  'F'.encode('ascii'), 'I'.encode('ascii'))
                time.sleep(1)
        robots[robot_id].write(mes)

    # ### TODO топология с менеджерами
    # for i in range(len(robots)):
    #     if robot_id == 0:
    #         if i == 0:
    #             mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), '0'.encode('ascii'),
    #                               'L'.encode('ascii'), 'S'.encode('ascii'))
    #             robots[robot_id].write(mes)
    #         elif i in rel.get(robot_id):
    #             mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), str(i).encode('ascii'),
    #                               'F'.encode('ascii'), 'I'.encode('ascii'))
    #             robots[robot_id].write(mes)
    #
    #     else:
    #         if i in rel and robot_id in rel.get(i):
    #             mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'),  str(i).encode('ascii'),
    #                               'L'.encode('ascii'), 'I'.encode('ascii'))
    #             robots[robot_id].write(mes)
    #         elif i == robot_id:
    #             if i in rel:
    #                 mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), str(i).encode('ascii'),
    #                                   'M'.encode('ascii'), 'S'.encode('ascii'))
    #                 robots[robot_id].write(mes)
    #             else:
    #                 mes = struct.pack("<s" + "s" + "ss" + "ss", 'l'.encode('ascii'), '4'.encode('ascii'), '0'.encode('ascii'), str(i).encode('ascii'),
    #                                   'F'.encode('ascii'), 'S'.encode('ascii'))
    #                 robots[robot_id].write(mes)
    #
    #     time.sleep(1)

    # timer = time.time()

    # lock.notify()
    # lock.release()
    #
    ### fixme Топология ЗВЕЗДА, начинают с линии

    if robot_id == 0:
        message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
                              '0060'.encode('ascii'))
        robots[0].write(message)

        time.sleep(0.9)

        # message3 = struct.pack("<s" + "s" + "4s" + "4s", 'm'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
        #                       '0060'.encode('ascii'))
        # robots[0].write(message3)
    elif robot_id == 1:
        message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
                              '0040'.encode('ascii'))
        robots[robot_id].write(message)

        time.sleep(0.95)

        message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '-015'.encode('ascii'),
                              '-015'.encode('ascii'))
        robots[robot_id].write(message)


    elif robot_id == 2:
        message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
                              '0020'.encode('ascii'))
        robots[robot_id].write(message)

        time.sleep(1.05)

        message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
                              '-015'.encode('ascii'))
        robots[robot_id].write(message)

    elif robot_id == 3:
        message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
                              '0000'.encode('ascii'))
        robots[robot_id].write(message)

        time.sleep(1.1)

        message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '-015'.encode('ascii'),
                              '-035'.encode('ascii'))
        robots[robot_id].write(message)

    elif robot_id == 4:
        message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
                              '-020'.encode('ascii'))
        robots[robot_id].write(message)

        time.sleep(1.15)

        message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
                              '-035'.encode('ascii'))
        robots[robot_id].write(message)


    # ### fixme Топология ЗВЕЗДА, начинают с формы
    #
    # if robot_id == 0:
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
    #                           '0060'.encode('ascii'))
    #     robots[0].write(message)
    #
    #     time.sleep(0.9)
    #
    #     # # message3 = struct.pack("<s" + "s" + "4s" + "4s", 'm'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
    #     # #                       '0060'.encode('ascii'))
    #     # robots[0].write(message3)
    # elif robot_id == 1:
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '-015'.encode('ascii'),
    #                           '0045'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    #     time.sleep(0.95)
    #
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '-015'.encode('ascii'),
    #                           '-015'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    #
    # elif robot_id == 2:
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
    #                           '0045'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    #     time.sleep(1.05)
    #
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
    #                           '-015'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    # elif robot_id == 3:
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '-015'.encode('ascii'),
    #                           '0025'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    #     time.sleep(1.1)
    #
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '-015'.encode('ascii'),
    #                           '-035'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    # elif robot_id == 4:
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
    #                           '0025'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    #     time.sleep(1.15)
    #
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
    #                           '-035'.encode('ascii'))
    #     robots[robot_id].write(message)





    #     ### fixme Топология с менеджерами, начинают с формы
    #
    # if robot_id == 0:
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
    #                           '0060'.encode('ascii'))
    #     robots[0].write(message)
    #
    #     time.sleep(0.9)
    #
    #     # # message3 = struct.pack("<s" + "s" + "4s" + "4s", 'm'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
    #     # #                       '0060'.encode('ascii'))
    #     # robots[0].write(message3)
    # elif robot_id == 1:
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '-015'.encode('ascii'),
    #                           '0045'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    #     time.sleep(0.95)
    #
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '-015'.encode('ascii'),
    #                           '-015'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    #
    # elif robot_id == 2:
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
    #                           '0045'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    #     time.sleep(1.05)
    #
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
    #                           '-015'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    # elif robot_id == 3:
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '-015'.encode('ascii'),
    #                           '0025'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    #     time.sleep(1.1)
    #
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
    #                           '-020'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    # elif robot_id == 4:
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
    #                           '0025'.encode('ascii'))
    #     robots[robot_id].write(message)
    #
    #     time.sleep(1.15)
    #
    #     message = struct.pack("<s" + "s" + "4s" + "4s", 'S'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
    #                           '-020'.encode('ascii'))
    #     robots[robot_id].write(message)



    time.sleep(2)

    # sent = 1

    # if "x" not in message:
    #     print("{} message start".format(robot_id), message)
    # robots[robot_id].write(message)

    if robot_id == 0:
        print ('wait')
        time.sleep(1)
        # message = struct.pack("<s" + "s" + "4s" + "4s", 'm'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
        #                   '0180'.encode('ascii'))
        message = struct.pack("<s" + "s" + "4s" + "4s", 'M'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
                              '0250'.encode('ascii'))
        robots[0].write(message)
        print ('sent--------------------')

    while loop:
        # try:
        reply = robots[robot_id].read()
        # except Exception:
        #     print("can't read from robot {}".format(robot_id))
        #     loop = False

            # if robot_id == 1:
            #     if time.time() - timer > 10 and sent == 1:
            #         message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0000'.encode('ascii'),
            #                               '0060'.encode('ascii'))
            #         robots[0].write(message)
            #         sent = 0
            #         print("-----------------------Send next message ----------------------")


        if reply == "d":
            num = read_n_chars(2, robots[robot_id])

            # print("{} num: ".format(robot_id), num)

            reply = read_n_chars(int(num), robots[robot_id])

            write_to_file(robot_id, reply, lock)

            # print("{} end reply: ".format(robot_id), reply)

        elif reply == "n":
                size = read_n_chars(1, robots[robot_id])
                print("{} size".format(robot_id), size)

                coordinates = read_n_chars(int(size), robots[robot_id])
                print(coordinates)

                if check_number(coordinates):

                    message = struct.pack("<s" + "s" + size + "s", 'n'.encode('ascii'), size.encode('ascii'), coordinates)
                    print("{} message with n".format(robot_id), message)

                    for robot_itt in range(len(robots)):
                        if robot_itt != robot_id:
                            if "x" not in message:
                                robots[robot_itt].write(message)
                                time.sleep(0.2)
                                print("send to robot {} mes:".format(robot_itt), message)

        elif reply == "N":
            size = read_n_chars(1, robots[robot_id])
            # print("{} size".format(robot_id), size)

            coordinates = read_n_chars(12, robots[robot_id])

            write_to_file(robot_id, coordinates, lock)

            print(coordinates)

            if check_number(coordinates):

                message = struct.pack("<s" + "s" + "12" + "s", 'N'.encode('ascii'), '<'.encode('ascii'), coordinates)
                print("{} message with N".format(robot_id), message)

                for robot_itt in rel.get(robot_id):
                    if robot_itt != robot_id:
                        if "x" not in message:
                            robots[robot_itt].write(message)
                            time.sleep(0.2)
                            print("send to robot {} mes:".format(robot_itt), message)

        reply = ""


def run_single_thread(robot):

    lock = threading.Lock()

    reply = ""

    message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
                          '0000'.encode('ascii'))
    robot.write(message)

    time.sleep(1)

    print('Введите X в формате: #### (см)')
    x = raw_input()

    print('Введите Y в формате: #### (см)')
    y = raw_input()

    # message = struct.pack("<s" + "s" + "4s" + "4s", 's'.encode('ascii'), '8'.encode('ascii'), '0015'.encode('ascii'),
    #                       '0000'.encode('ascii'))
    # robot.write(message)

    # time.sleep(1)
    #
    message = struct.pack("<s" + "s" + "4s" + "4s", 'm'.encode('ascii'), '8'.encode('ascii'), str(x).encode('ascii'),
                          str(y).encode('ascii'))

    robot.write(message)

    reply = ""
    itt = 0
    timer = time.time()

    reply = ""
    while True:

        if time.time() - timer > 2 and itt > 0:
            robot.write("f")
            itt = 0

        reply = robot.read()
        if reply == "d":
            itt += 1
            timer = time.time()

            reply = robot.read()

            while len(reply) < 2:
                reply += robot.read()

            num = reply
            reply = ""
            print("num: ", num)
            for i in range(int(num)):
                reply += robot.read()

            print("end reply: ", reply)

            # write_to_file(01, reply, lock)


            if reply == "add_mes":
                    print("reply == 'add_mes'")
                    m = struct.pack("s", "f")
                    robot.write("f")


robot_numbers = 3
threads = []
robots = []
# serial_ports_array = ['/dev/cu.e-puck2_04213-UART'] #, '/dev/cu.e-puck2_04213-UART']
serial_ports_array = ['/dev/cu.e-puck2_04213-UART', '/dev/cu.e-puck2_04214-UART', '/dev/cu.e-puck2_04215-UART']
# serial_ports_array = ['/dev/cu.e-puck2_04213-UART', '/dev/cu.e-puck2_04214-UART', '/dev/cu.e-puck2_04215-UART', '/dev/cu.e-puck2_04216-UART', '/dev/cu.e-puck2_04217-UART']
error_loaded = []

for i in range(robot_numbers):
    try:
        robots.append(serial.Serial(serial_ports_array[i], 115200,
                                    timeout=0))  # Specify the robot communication port (after pairing).
    except Exception:
        error_loaded.append(i)
        print("cannot connect to robot i:", i)

if len(error_loaded) == 0:
    for i in range(robot_numbers):
        threads.append(MyThread(i, robots))

    for i in range(robot_numbers):
        threads[i].start()

    for i in range(robot_numbers):
        threads[i].join()

for i in range(robot_numbers):
    if not (i in error_loaded):
        robots[i].close()

print "Exiting Main Thread"
