import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import datetime


def make_plot(robots, show=True):
    fig, ax = plt.subplots(1,1, figsize=(20,12))

    # lw = 2
    colors = ['b', 'g', 'r', 'black','gold']
    markers = ['o', '+', 'v', '*', '^']
    cmaps = ['Blues', 'Greens', 'Reds', 'Greys']
    np_robots = np.array([])

    #todo think
    # # robots[2][:,1] += 5
    # # robots[1][:,1] += 5
    # # robots[2][32:,1] += 4
    #
    # # robots[1][:,0] += 30
    # robots[3][14:,1] += 3
    # robots[4][15:,1] += 7
    # robots[4][21:38,0] -= 5
    # robots[4][20,0] -= 2.5
    # robots[4][38,0] -= 2.5



    for i in range(len(robots)):
        if len(robots[i]):
            np_robots = np.append(np_robots, robots[i])
            ax.plot(robots[i][:, 1] - 60, robots[i][:, 0] * -1,
                    color=colors[i],
                    marker=markers[i],
                    linestyle='--',
                    label='robot {}'.format(i))

    #example
    # rect = patches.Rectangle((0,10), 10, 10, angle=-45, linewidth=1,edgecolor='none',facecolor='pink', fill=True)
    # ax.add_patch(rect)

    # Major ticks every 20, minor ticks every 5
    x_major_ticks = np.arange(-50, 201, 10)
    x_minor_ticks = np.arange(-50, 201, 5)

    y_major_ticks = np.arange(-22, 25, 2)
    # y_minor_ticks = np.arange(-10, 6, 2.5)

    ax.set_xlim(-50, 200)
    ax.set_ylim(-22, 24)

    ax.set_xticks(x_major_ticks)
    ax.set_xticks(x_minor_ticks, minor=True)
    ax.set_yticks(y_major_ticks)
    # ax.set_yticks(y_minor_ticks, minor=True)

    ax.grid(which='both')

    ax.grid(which='minor', alpha=0.2)
    # ax.grid(which='major', alpha=0.5)


    # ax.grid(True)
    ax.set_xlabel('Y', fontsize=30)
    ax.set_ylabel('X', fontsize=30)

    # ax.tick_params(axis = 'both', which = 'major', labelsize = 24)
    # ax.tick_params(axis = 'both', which = 'minor', labelsize = 16)

    # ax.set_title('robots movement')
    # ax.legend(loc="lower right")
    ax.legend(loc=(0.002, 0.3))
    maxim = np.max(np_robots)
    minim = np.min(np_robots)


    plt.savefig('nothing_just_fun_1' + '-' + str(datetime.datetime.now()) + '.png')
    if show:
        plt.show()


def parse_message(message):
    if len(message) < 8:
        return -1

    x = int(message[0:4])
    y = int(message[4:8])
    return np.array([x, y])


def parse_line(line):
    if len(line) < 10:
        return -1
    i = line[0]
    if i < '0' or i > '9' or line[1] != ' ':
        return -1
    if type(parse_message(line[2:])) == int:
        return -1
    return int(i), parse_message(line[2:])


def read_from_file_and_do_everything(file_name, show_plot=False):
    file = open(file_name, 'r')
    all_robots = [[], [], [], [], []]
    for line in file.readlines():
        if type(parse_line(line)) != int:
            i, coord = parse_line(line)
            all_robots[i].append(coord)

    for i in range(len(all_robots)):
        all_robots[i] = np.array(all_robots[i])

    make_plot(all_robots, show_plot)
    file.close()


def generate_example(file_name):
    file_try = open(file_name, 'w+')
    for i in range(100):
        string = str(i)
        if len(string) < 2:
            string += '0'
        file_try.write('0 800'+string+'00' + string+'\n')

    file_try.close()


plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams['font.size'] = 26
plt.rc('text', usetex=True)
plt.rc('text.latex',unicode=True)
plt.rc('text.latex',preamble='\\usepackage[utf8]{inputenc}')
plt.rc('text.latex',preamble='\\usepackage[english]{babel}')
read_from_file_and_do_everything('nothing_just_fun_3.txt')
