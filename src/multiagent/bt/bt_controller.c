//
// Created by Смирнов Влад on 2019-01-28.
//

#include "sensors/VL53L0X/VL53L0X.h"
#include "audio/play_melody.h"
#include "audio/play_sound_file.h"
#include "button.h"
#include "leds.h"
#include "sdio.h"

#include <string.h>
#include <ctype.h>
#include <stdio.h>

#include <motor_led/e_epuck_ports.h>
#include <motor_led/e_init_port.h>
#include <motor_led/advance_one_timer/e_led.h>
#include <motor_led/advance_one_timer/e_motors.h>
#include <uart/e_uart_char.h>
#include <a_d/advance_ad_scan/e_ad_conv.h>
#include <a_d/advance_ad_scan/e_acc.h>
#include <a_d/advance_ad_scan/e_prox.h>
#include <a_d/advance_ad_scan/e_micro.h>
#include <motor_led/advance_one_timer/e_agenda.h>
#include <camera/fast_2_timer/e_po8030d.h>
#include <camera/fast_2_timer/e_poxxxx.h>
#include <codec/e_sound.h>
#include <utility/utility.h>
#include <acc_gyro/e_lsm330.h>
#include "ch.h"
#include "chprintf.h"
#include "hal.h"
#include "shell.h"

#include "aseba_vm/aseba_node.h"
#include "aseba_vm/skel_user.h"
#include "aseba_vm/aseba_can_interface.h"
#include "aseba_vm/aseba_bridge.h"
#include "audio/audio_thread.h"
#include "audio/play_melody.h"
#include "audio/play_sound_file.h"
#include "audio/microphone.h"
#include "camera/po8030.h"
#include "epuck1x/Asercom.h"
#include "epuck1x/Asercom2.h"
#include "epuck1x/a_d/advance_ad_scan/e_acc.h"
#include "epuck1x/motor_led/advance_one_timer/e_led.h"
#include "epuck1x/utility/utility.h"
#include "sensors/battery_level.h"
#include "sensors/imu.h"
#include "sensors/mpu9250.h"
#include "sensors/proximity.h"
#include "sensors/VL53L0X/VL53L0X.h"
#include "button.h"
#include "cmd.h"
#include "config_flash_storage.h"
#include "exti.h"
#include "fat.h"
#include "i2c_bus.h"
#include "ir_remote.h"
#include "leds.h"
#include <main.h>
#include "memory_protection.h"
#include "memory.h"
#include "motors.h"
#include "sdio.h"
#include "selector.h"
#include "spi_comm.h"
#include "usbcfg.h"
#include "communication.h"
#include "uc_usage.h"

#include "multiagent/model/data_models.h"
#include "multiagent/movement/movement_controller.h"
#include "bt_controller.h"
#include "math.h"
#include "../model/data_models.h"

#include <../../ChibiOS/os/hal/lib/streams/chprintf.h>



#define IR_RECEIVER

#define uart1_send_static_text(msg) do { e_send_uart1_char(msg,sizeof(msg)-1); while(e_uart1_sending()); } while(0)
#define uart1_send_text(msg) do { e_send_uart1_char(msg,strlen(msg)); while(e_uart1_sending()); } while(0)
#define uart2_send_static_text(msg) do { e_send_uart2_char(msg,sizeof(msg)-1); while(e_uart2_sending()); } while(0)
#define uart2_send_text(msg) do { e_send_uart2_char(msg,strlen(msg)); while(e_uart2_sending()); } while(0)

#define MAX_MES_NUM 10
#define MAX_AGENTS  10
#define MES_TIMEOUT 1000

//extern char buffer[BUFFER_SIZE];
extern int selector; //extern int selector;
extern char c;

int read_mes(Message* message);
void init_bt_protocol(Robot*robot);
void run_protocol(Robot* robot);
void send_message(Robot* robot);
int add_message(Robot* robot, Message* message);
int process_message(Robot* robot, Message* message);
int string_to_double(char* message, double* v, int from, int to);
int double_to_string(double num, char* message, int from, int to);


void init_bt_protocol(Robot* robot) {

    e_acc_calibr ();

    Agent* agents = (Agent*)malloc(sizeof(Agent) * MAX_AGENTS);

    robot->agents = agents;
    robot->agents_num = 0;
    robot->leader_id = -1;

    robot->message_num = 0;
    robot->messages = malloc(sizeof(Message) * MAX_MES_NUM);
}

void run_protocol(Robot* robot) {

    if (e_getchar_uart1(&c) != 0) {

        if (c == 'f' || c == 0 || c == '0') {

            return;
        } else if ((int8_t) c < 0) { // bytes

        } else if (c > 0) { // ascii code

            Message message;
            message.type = c;

            if (read_mes(&message) == 1) {

                process_message(robot, &message);
            }
        }
    }

//    send_message(robot);

}

int read_mes(Message* message) {

    systime_t time = chVTGetSystemTime();

    if (SDU1.config->usbp->state == 4) {
        chprintf ((BaseSequentialStream *) &SDU1,
                  "------------------read_message_%c\r\n", message->type);
    }

    while (!e_getchar_uart1(&c)) {
        if (chVTGetSystemTime() > time + MES_TIMEOUT) {
            return -1;
        }
    }


    message->size = c - '0';
    if (SDU1.config->usbp->state == 4) {
        chprintf ((BaseSequentialStream *) &SDU1,
                  "------------------size_%d------------------\r\n", message->size);
    }

    if (message->size == 0) {
        if (SDU1.config->usbp->state == 4) {
            chprintf ((BaseSequentialStream *) &SDU1,
                      "------------------bad_end_read_message------------------\r\n");
        }
        return -1;
    }


    char* message_text = malloc(sizeof(char) * (message->size + 2));
    message_text[0] = message->type;
    message_text[1] = message->size +'0';

    for (int i = 2; i < message->size + 2; i++) {
        while (!e_getchar_uart1(&c)) {
            if (chVTGetSystemTime() > time + MES_TIMEOUT) {
                if (SDU1.config->usbp->state == 4) {
                    chprintf ((BaseSequentialStream *) &SDU1,
                              "------------------bad_end_read_message------------------\r\n");
                }
                return -1;
            }
        }
        message_text[i] = c;
    }

    message->message = message_text;
    if (SDU1.config->usbp->state == 4) {
        for (int i = 0; i < message->size + 2; i++) {
            chprintf((BaseSequentialStream * ) & SDU1,
                     "%c", message->message[i]);
        }
        chprintf((BaseSequentialStream * ) & SDU1,
                 "\r\n");
    }

    free(message_text);

    if (SDU1.config->usbp->state == 4) {
        chprintf ((BaseSequentialStream *) &SDU1,
                  "------------------end_read_message------------------\r\n");
    }

    return 1;
}

void send_message(Robot* robot) {

    uart1_send_static_text("d09read_mes1");
    if (robot->message_num > 0) {

        uart1_send_static_text("d09read_mes2");
        int i;
        Message* messages = robot->messages;
        for (i = 0; i < robot->message_num; i++) {

            uart1_send_static_text("d09read_mes3");
            switch (messages[i].type) {

                case 'd':
                    switch (robot->mode) {
                        case 'm':
                            e_send_uart1_char (messages[i].message, messages[i].size);
                            while(e_uart1_sending());
                            free(messages[i].message);

                        break;
                    }
                    break;

                default:

                    break;
            }
        }

        free(robot->messages);
        robot->messages = malloc(sizeof(Message) * MAX_MES_NUM);
        robot->message_num = 0;
    }
}

int process_message(Robot* robot, Message* message) {

    int num;
    double x = 0;
    double y = 0;
    double angle = 0.0;
    int var_size; // = message->size / 2;

    if (SDU1.config->usbp->state == 4) {
        chprintf ((BaseSequentialStream *) &SDU1,
                  "------------------process_message------------------\r\n");
    }

    switch (message->type) {

        case 'm':
            var_size = message->size / 2;

            if (!string_to_double(message->message, &x, 2, 2 + var_size)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &y, 2 + var_size, 2 + var_size * 2)) {
                free(message->message);
                return -1;
            }


            set_goal_coordinates(robot, x/100, y/100); //мне привычнее пока в см отправлять


            if (robot->leader_id == 0) {
                robot->debug.type = 'n';
            } else {
                robot->debug.type = 'm';
            }


            robot->mode = 'm';
            message->type = '0';
//            free(message->message);

            return 1;

        case 'M':
            var_size = message->size / 2;

            if (!string_to_double(message->message, &x, 2, 2 + var_size)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &y, 2 + var_size, 2 + var_size * 2)) {
                free(message->message);
                return -1;
            }


            set_goal_coordinates(robot, x/100, y/100); //мне привычнее пока в см отправлять


            if (robot->leader_id == 0) {
                robot->debug.type = 'N';
            } else {
                robot->debug.type = 'M';
            }


            robot->mode = 'M';
            message->type = '0';
//            free(message->message);

            return 1;

        case 's':
            var_size = message->size / 2;

            if (!string_to_double(message->message, &x, 2, 2 + var_size)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &y, 2 + var_size, 2 + var_size * 2)) {
                free(message->message);
                return -1;
            }

            set_own_coordinates(robot, x/100, y/100);
            set_goal_coordinates(robot, x/100, y/100);

//            robot->mode = 'm';
//
//            if (robot->leader_id == 0) {
//                robot->debug.type = 'n';
//            } else {
//                robot->debug.type = 'm';
//            }

            message->type = '0';

            return 1;

        case 'S':
            var_size = message->size / 2;

            if (!string_to_double(message->message, &x, 2, 2 + var_size)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &y, 2 + var_size, 2 + var_size * 2)) {
                free(message->message);
                return -1;
            }

            set_shift_coordinates(robot, x/100, y/100);

            if (SDU1.config->usbp->state == 4) {
                chprintf ((BaseSequentialStream *) &SDU1,
                          "mes[5]=%c\r\n", message->message[5]);
            }

            message->type = '0';

            return 1;

        case 'n':
            var_size = message->size / 2;

            if (!string_to_double(message->message, &x, 2, 2 + var_size)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &y, 2 + var_size, 2 + var_size * 2)) {
                free(message->message);
                return -1;
            }


            robot->joint_movement.consider = false;

            set_goal_coordinates (robot, x/100, y/100);

            robot->debug.type = 'm';
            robot->mode = 'm';

            message->type = '0';

            return 1;

        case 'N':
            var_size = message->size / 3;

            if (!string_to_double(message->message, &x, 2, 2 + var_size)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &y, 2 + var_size, 2 + var_size * 2)) {
                free(message->message);
                return -1;
            }

            if (!string_to_double(message->message, &angle, 2 + var_size * 2, 2 + var_size * 3)) {
                free(message->message);
                return -1;
            }

            robot->joint_movement.consider = true;
            robot->movement.target_theta = angle / 100.0;
            set_goal_coordinates (robot, x / 100.0, y / 100.0);

            if (robot->is_manager) {
                robot->debug.type = 'N';
            } else {
                robot->debug.type = 'M';
            }
            robot->mode = 'M';

            message->type = '0';

            return 1;

        case 'l':
            var_size = message->size / 2;

            if (message->size != 4) {
                return -1;
            }
            num = (message->message[2] - '0') * 10 + message->message[3] - '0';

            if (SDU1.config->usbp->state == 4) {
                chprintf ((BaseSequentialStream *) &SDU1,
                          "num=%d\r\n", num);
            }

            Agent agent;

            if (num >= 0 && num < 100) {
                agent.s_num = num;
            } else {
                return -1;
            }

            if (SDU1.config->usbp->state == 4) {
                chprintf ((BaseSequentialStream *) &SDU1,
                          "mes[4]=%c\r\n", message->message[4]);
            }

            if (message->message[4] == 'L') { /// L - Leader
                agent.isLeader = true;
            } else if (message->message[4] == 'F') { /// F - Follower
                agent.isLeader = false;
            } else if (message->message[4] == 'M') { /// M - Manager

                if (message->message[5] == 'S') {
                    robot->is_manager = true;
                }
                agent.isLeader = false;
            } else {
                return -1;
            }

            if (SDU1.config->usbp->state == 4) {
                chprintf ((BaseSequentialStream *) &SDU1,
                          "mes[5]=%c\r\n", message->message[5]);
            }

            if (message->message[5] == 'S') { /// S - Set

                robot->agents[0] = agent;
                if (agent.isLeader) {
                    robot->leader_id = 0;
                }
            } else if (message->message[5] == 'I') { /// I - Info

                robot->agents[++robot->agents_num] = agent;
                if (agent.isLeader) {
                    robot->leader_id = robot->agents_num;
                }
            } else {
                return -1;
            }

            return 1;
    }

    robot->debug.type = '0';
    robot->mode = '0';
    free(message->message);
    return -1;

}

int add_message(Robot* robot, Message* message) {

    if (robot->message_num < MAX_MES_NUM) {
        robot->messages[robot->message_num++] = *message;

        uart1_send_static_text("d07add_mes");

        return 1;
    }

    return 0;
}

/**
 * Parse string to double, sensitive to sign and dot
 * @param message - string
 * @param v - result variable
 * @param from - start of number in message
 * @param to - end of number in message
 * @return 1 - everything ok, -1 - parsing error
 */
int string_to_double(char* message, double* v, int from, int to) {

    double x = *v;

    char mes_el;
    short dot_flag = 0;
    int negative = 1;
    for (int i = from; i < to; i++) {
        mes_el = message[i];

        if (mes_el >= '0' && mes_el <= '9') {
            if (dot_flag) {
                x = x + ((double)(mes_el - '0')) / pow(10, dot_flag);
                dot_flag++;
            } else {
                x = x * 10 + (mes_el - '0');
            }
        }

        if (mes_el == '.') {
            if (dot_flag == 0) {
                dot_flag++;
            } else {
                return -1;
            }
        }

        if (mes_el == '-') {
            if (i == from) {
                negative = -1;
            } else {
                return -1;
            }
        }

        if (mes_el == '+') {
            if (i == from) {
                negative = 1;
            } else {
                return -1;
            }
        }
    }
    x = x * negative;

    *v = x;

    return 1;

}

/**
 * Convert double to string, it takes meters and converts to centimeters
 * @param num double to be parsed
 * @param message where put string
 * @param from
 * @param to
 * @return 1 - everything ok, -1 - parsing error
 */
int double_to_string(double num, char* message, int from, int to) {

    int int_num = round(num * 100);
    int is_negative = 0;

    if (int_num < 0) {

        is_negative = 1;
        int_num = -int_num;

    }

    if (pow(10, to - from - 1 - is_negative) <= int_num) {

        return -1;
    }

    int n;
    int i = from;

    if (is_negative) {

        message[i++] = '-';
    }

    for (; i < to; i++) {

        n = (int)(int_num / pow(10, to - (i + 1))) % 10;
        message[i] = '0' + n;
    }


    return 1;
}

//int run_protocol2(Robot robot) {
//
//    if (SDU1.config->usbp->state == 4) {
//        chprintf((BaseSequentialStream *) &SDU1, "in asercom\n");
//    }
//
//    static char c1, c2, wait_cam = 0;
//    static int i, j, n, speedr, speedl, positionr, positionl, LED_nbr, LED_action, accx, accy, accz, sound, gyrox, gyroy, gyroz;
//    static int cam_mode, cam_width, cam_heigth, cam_zoom, cam_size, cam_x1, cam_y1;
//    static char first = 0;
//    static char rgb_value[12];
//    char *ptr;
//    static int mod, reg, val;
//#ifdef IR_RECEIVER
//    char ir_move = 0, ir_address = 0, ir_last_move = 0;
//#endif
//    static TypeAccSpheric accelero;
//    //static TypeAccRaw accelero_raw;
//    int use_bt = 0;
//    unsigned int battValue = 0;
//    uint8_t gumstix_connected = 0;
//    uint16_t cam_start_index = 0;
//    char cmd = 0;
//    float tempf = 0.0;
//    uint32_t tempi = 0;
//
//    //e_init_port();    // configure port pins
//    //e_start_agendas_processing();
//    e_init_motors();
//    //e_init_uart1();   // initialize UART to 115200 Kbaud
//    //e_init_ad_scan();
//
//    selector = getselector(); //SELECTOR0 + 2*SELECTOR1 + 4*SELECTOR2 + 8*SELECTOR3;
//    printNumberToPort(selector);
//    printStringToPort("selector");
//    use_bt = 1; // new comments: selector == 3 for bt (at least in my case)
//
//    gumstix_connected = 0;
//
//
//#ifdef FLOOR_SENSORS
//    if (gumstix_connected == 0) { // the I2C must remain disabled when using the gumstix extension
//        e_i2cp_init();
//    }
//#endif
//
//#ifdef IR_RECEIVER
//    e_init_remote_control();
//#endif
////    if (RCONbits.POR) { // reset if power on (some problem for few robots)
////        RCONbits.POR = 0;
////        RESET();
////    }
//    /*read HW version from the eeprom (last word)*/
//    static int HWversion = 0xFFFF;
//    ReadEE(0x7F, 0xFFFE, &HWversion, 1);
//
////    /*Cam default parameter*/
////    cam_mode = RGB_565_MODE;
////    //cam_mode=GREY_SCALE_MODE;
////    cam_width = 40; // DEFAULT_WIDTH;
////    cam_heigth = 40; // DEFAULT_HEIGHT;
////    cam_zoom = 8;
////    cam_size = cam_width * cam_heigth * 2;
////
////    e_poxxxx_init_cam();
////    e_poxxxx_config_cam((ARRAY_WIDTH - cam_width * cam_zoom) / 2, (ARRAY_HEIGHT - cam_heigth * cam_zoom) / 2,
////                        cam_width * cam_zoom, cam_heigth * cam_zoom, cam_zoom, cam_zoom, cam_mode);
////    e_poxxxx_write_cam_registers();
//
//
//    e_acc_calibr();
//    uart1_send_static_text("\f\a"
//                           "WELCOME to the SerCom protocol on e-Puck\r\n"
//                           "the EPFL education robot type \"H\" for help\r\n");
//
//
//    printStringToPort("before while");
//    while (1) {
//
//         // Communicate with ESP32 (uart) => BT.
//            //printStringToPort("Communicate with ESP32 (uart) => BT.");
//            //printStringToPort("e_getchar_uart1(&c)");
//            if (SDU1.config->usbp->state == 4) {
//                //chprintf((BaseSequentialStream *) &SDU1, "--->%c<----->%p<-----\n", c, &c);
//            }
//            //printStringToPort("e_getchar_uart1(&c)");
//
//            while (e_getchar_uart1(&c) == 0);
//
//
//        if ((int8_t) c < 0) { // binary mode (big endian)
//            //new comments - come here
//            //printStringToPort("// binary mode (big endian)");
//            i = 0;
//
//            do {
////                switch ((int8_t) -c) {
////
////
////                    case 'X':
////                        ;
////                        //Время для шага итерации:
////                        systime_t T_nach; //Начало временного промежутка [мс].
////                        //Флаг:
////                        uint8_t Potential_method_switch = 0; //Включение и отключение метода потенциалов.
////
////
////
////
////                        if (use_bt) { // Communicate with ESP32 (uart) => BT.
////                            while (e_getchar_uart1(&c1) == 0);
////                        }
////                        //printStringToPort("in case X");
////
////                        switch (c1) {
////                            case 2 :
////                                // printStringToPort("in case X get c1=0000");
////                                left_motor_set_pos(0);
////                                right_motor_set_pos(0);
////
////                                Right_encoder_old = (double) right_motor_get_pos();
////                                Left_encoder_old = (double) right_motor_get_pos();
////
////                                Update_odometer1(&Left_encoder_old, &Right_encoder_old, &Theta, &X_pos_actual,
////                                                 &Y_pos_actual);
////
////                                right_motor_set_speed(300);
////                                left_motor_set_speed(-300);
////
////
////                                initTheta = Theta;
////                                while (fabs((Theta - initTheta) * 180 / M_PI) < 20) {
//////                                printNumberToPort(left_motor_get_pos());
////                                    // if (SDU1.config->usbp->state == 4) {
////
//////                                    Right_encoder_old = (double) right_motor_get_pos();
//////                                    Left_encoder_old = (double) right_motor_get_pos();
////
////                                    Update_odometer1(&Left_encoder_old, &Right_encoder_old, &Theta, &X_pos_actual,
////                                                     &Y_pos_actual);
////
////
//////                                    chprintf((BaseSequentialStream *) &SDU1,
//////                                             "Theta = %8.4lf, X =  %8.4lf, Y =  %8.4lf, condition = %8.4lf\r\n",
//////                                             Theta * 180 / M_PI,
//////                                             X_pos_actual, Y_pos_actual, abs((Theta - initTheta) * 180 / M_PI));
////                                    // }
////                                }
////                                right_motor_set_speed(0);
////                                left_motor_set_speed(0);
////                                buffer[i++] = 'r';
////                                buffer[i++] = 'r';
////                                buffer[i++] = 'r';
////                                buffer[i++] = 'r';
////                                printStringToPort("in case X send rrrrr");
////                                break;
////                            case 1:
////                                printStringToPort("in case X get c1=1111");
////
////                                left_motor_set_pos(0);
////                                right_motor_set_pos(0);
////
////
////                                Right_encoder_old = (double) right_motor_get_pos();
////                                Left_encoder_old = (double) right_motor_get_pos();
////
////                                Update_odometer2(&Left_encoder_old, &Right_encoder_old, &Theta, &X_pos_actual,
////                                                 &Y_pos_actual);
////
////                                right_motor_set_speed(-300);
////                                left_motor_set_speed(300);
////
////
////                                initTheta = Theta;
////                                while (fabs((Theta - initTheta) * 180 / M_PI) < 20) {
//////                                printNumberToPort(left_motor_get_pos());
////                                    // if (SDU1.config->usbp->state == 4) {
////
//////                                    Right_encoder_old = (double) right_motor_get_pos();
//////                                    Left_encoder_old = (double) right_motor_get_pos();
////
////                                    Update_odometer2(&Left_encoder_old, &Right_encoder_old, &Theta, &X_pos_actual,
////                                                     &Y_pos_actual);
////
////
//////                                    chprintf((BaseSequentialStream *) &SDU1,
//////                                             "Theta = %8.4lf, X =  %8.4lf, Y =  %8.4lf, condition = %8.4lf\r\n",
//////                                             Theta * 180 / M_PI,
//////                                             X_pos_actual, Y_pos_actual, abs((Theta - initTheta) * 180 / M_PI));
////                                    // }
////                                }
////
////                                right_motor_set_speed(0);
////                                left_motor_set_speed(0);
////                                buffer[i++] = 'l';
////                                buffer[i++] = 'l';
////                                buffer[i++] = 'l';
////                                buffer[i++] = 'l';
////                                printStringToPort("in case X send llll");
////                                break;
////
////                            case 4:
////                                //Робот вычисляет вектор отталкивания и поворачивается в соответствии с ним.
////                                Theta = M_PI/2;
////                                while(1)
////                                {
////                                    Filter_and_update_proximity_values(Prox_0_sens_values, Prox_1_sens_values, Prox_2_sens_values, Prox_5_sens_values, Prox_6_sens_values, Prox_7_sens_values, Size, Filtered_proximity_value);
////                                    for(i = 0; i < 6; i++)
////                                    {
////                                        Proximity_values_mm[i] = Get_proximity_value_mm(Filtered_proximity_value[i]);
////                                    }
////                                    Update_obstacle_repulsion_vector(Proximity_values_mm, Theta, Sensor_angles, &Obstacle_repulsion_vector_angle, &Obstacle_repulsion_vector_length);
////                                    Goal_angle = Obstacle_repulsion_vector_angle;
////                                    Goal_point_distance = 0;
////
////                                    for(j = 0; j < 30; j++)
////                                    {
////                                        Update_odometer2(&Left_encoder_old, &Right_encoder_old, &Theta, &X_pos_actual, &Y_pos_actual);
////                                        Update_angle_disagreement(&Angle_disagreement, Theta, Goal_angle);
////                                        Update_goal_speeds_values(&Linear_speed_goal, &Angular_speed_goal, Goal_point_distance, Dec_radius, Stop_radius, Linear_speed_max, Angle_disagreement, Angular_speed_max, Dec_angle, Potential_method_switch, Moving_speed_value_potential_method);
////                                        Calculate_speeds(&Linear_speed_real, &Angular_speed_real, Linear_acc, Angular_acc, Linear_speed_goal, Angular_speed_goal);
////                                        Set_wheels_speeds(Linear_speed_real, Angular_speed_real, h);
////                                        chThdSleepMilliseconds(95);
////
////                                        if (SDU1.config->usbp->state == USB_ACTIVE)
////                                        {
////                                            chprintf((BaseSequentialStream *)&SDU1, "Theta = %lf, Obstacle repulse angle = %lf\r\n", Theta*180/M_PI, Obstacle_repulsion_vector_angle*180/M_PI);
////                                        }
////                                    }
////                                }
////                                break;
////                            case 5:
////                                //Тест - в порт выводится угол и модуль вектора отталкивания.
////                                while(1)
////                                {
////
////                                    Filter_and_update_proximity_values(Prox_0_sens_values, Prox_1_sens_values, Prox_2_sens_values, Prox_5_sens_values, Prox_6_sens_values, Prox_7_sens_values, Size, Filtered_proximity_value);
////
////                                    for(i = 0; i < 6; i++)
////                                    {
////                                        Proximity_values_mm[i] = Get_proximity_value_mm(Filtered_proximity_value[i]);
////                                    }
////                                    Update_obstacle_repulsion_vector(Proximity_values_mm, Theta, Sensor_angles, &Obstacle_repulsion_vector_angle, &Obstacle_repulsion_vector_length);
////                                    chprintf((BaseSequentialStream *)&SDU1, "Angle = %lf, Module = %lf\r\n", Obstacle_repulsion_vector_angle*180/M_PI, Obstacle_repulsion_vector_length);
////
////                                    chThdSleepMilliseconds(100);
////                                }
////                                break;
////                            case 6:
////                                //Движение к точке - БЕЗ метода потенциалов.
////                                Potential_method_switch = 0;
////                                Set_goal_coordinates(0.3, 0.2, &X_goal_point, &Y_goal_point); //Установка целевых координат [м][м].
////                                while(1) //Рабочий цикл для движения к целевой точке.
////                                {
////                                    T_nach = chVTGetSystemTime(); //Начало временного промежутка.
////
////                                    Update_odometer2(&Left_encoder_old, &Right_encoder_old, &Theta, &X_pos_actual, &Y_pos_actual); //Обновляет положение центра робота и его угол ориентации.
////                                    Update_goal_point_distance(&Goal_point_distance, X_pos_actual, Y_pos_actual, X_goal_point, Y_goal_point);
////                                    Update_goal_point_angle(&Goal_point_angle, X_pos_actual, Y_pos_actual, X_goal_point, Y_goal_point);
////                                    Goal_angle = Goal_point_angle;
////                                    Update_angle_disagreement(&Angle_disagreement, Theta, Goal_angle);
////                                    Update_goal_speeds_values(&Linear_speed_goal, &Angular_speed_goal, Goal_point_distance, Dec_radius, Stop_radius, Linear_speed_max, Angle_disagreement, Angular_speed_max, Dec_angle, Potential_method_switch, Moving_speed_value_potential_method);
////                                    Calculate_speeds(&Linear_speed_real, &Angular_speed_real, Linear_acc, Angular_acc, Linear_speed_goal, Angular_speed_goal);
////                                    Set_wheels_speeds(Linear_speed_real, Angular_speed_real, h);
////
////                                    if (SDU1.config->usbp->state == USB_ACTIVE)
////                                    {
////                                        chprintf((BaseSequentialStream *)&SDU1, "Theta = %8.3lf, X =  %8.3lf, Y =  %8.3lf\r\n", Theta*180/M_PI, X_pos_actual, Y_pos_actual);
////                                    }
////                                    while(chVTGetSystemTime() <  (T_nach + 100));
////                                }
////                                break;
////                        }
////
////                        e_send_uart1_char(buffer, i); // send answer
////                        while (e_uart1_sending());
////
////                        break;
////                    default: // silently ignored
////                        break;
////                }
//
//                while (e_getchar_uart1(&c) == 0); // get next command
//
//            } while (c);
//
//            // **** ascii mode ****
//        }
////    		else if (c > 0) { // ascii mode
////
////        }
//
//        printStringToPort("end of loop");
//    }
//    printStringToPort("end");
//}
