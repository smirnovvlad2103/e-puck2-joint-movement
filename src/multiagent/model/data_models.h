//
// Created by Смирнов Влад on 2019-01-29.
//

#ifndef E_PUCK2_MAIN_PROCESSOR_DATA_MODELS_H
#define E_PUCK2_MAIN_PROCESSOR_DATA_MODELS_H

#include "sensors/VL53L0X/VL53L0X.h"

#include <ch.h>


struct _Movement {

    double H;
    double AX_H;
    double MAX_VEL_LINE_WHEEL;
    double MAX_VEL_ANGLE_WHEEL;
    double MAX_VEL_LINE;
    double MAX_VEL_ANGLE;
    double ACC_LINE;
    double ACC_ANGLE;

    //// Описание робота:
    double x_real, y_real; //Реальные коорд-ты центра робота [м].
    double theta; //Угол ориентации относительно Ox [рад].
    double target_theta; //Угол ориентации относительно Ox [рад].
    double vel_line_real; //Реальная линейная скорость [м/с].
    double vel_angle_real; //Реальная угловая скорость [рад/с].
    double vel_left_ms, vel_right_ms; //Скорость лев. и прав. колес в [м/с].
    int16_t vel_left_steps, vel_right_steps; //Скорость лев. и прав. колес в [шаги/с].

    //// Описание целевой точки:
    double x_target, y_target; //Целевые коорд-ты центра робота [м].
    double stop_radius; //В этом радиусе от цели - стоп [м].
    double dec_radius; //В этом радиусе до цели - торможение [м].
    double dec_angle; //В этом угле начинается торможение [рад].
    double target_angle; //Угол на цель отн. оси Ox [рад].
    double target_distance; //Расстояние до цели [м].

    //// Целевые параметры робота:
    double vel_line_target; //Целевая линейная скорость [м/с].
    double vel_angle_target; //Целевая угловая скорость [рад/с].
    double target_step_angle; //Это условный целевой угол отн. оси Ox, который будет все время меняться [рад].
    double disagreement_angle; //Рассогласование по углу [рад].

    //// Переменные для работы колесной одометрии:
    double left_encoder_old; //Количество шагов на левом колесе пердыдущее [шаги].
    double right_encoder_old; //Количество шагов на правом колесе пердыдущее [шаги].

    //// Данные для реализации метода потенциальных сил:
    int potential_method_switch;
    //// Данные для получения вектора отталкивания от препятствий:
    int sens_val_num;
    int prox_0_sens_values[5]; //Значения, подаваемые в медианный фильтр для каждого сенсора.
    int prox_1_sens_values[5];
    int prox_2_sens_values[5];
    int prox_5_sens_values[5];
    int prox_6_sens_values[5];
    int prox_7_sens_values[5];
    int filtered_proximity_value[6]; //Отфильтрованные значения.
    double proximity_values_mm[6]; //Значения дальностей [мм].
    double obstacle_repulsion_vector_length;
    double obstacle_repulsion_vector_angle;
    double sensor_angles[6]; //Углы расположения ИК-датчиков на роботе (отн. поперечной оси) [рад].

    //// Данные для получения вектора отталкивания от других агентов:
    char n_robots; //Общее количество агентов (включая текущего).
    double agents_repulsion_vector_length;
    double agents_repulsion_vector_angle;

    //// Результирующий вектор движения:
    double vel_potential_method;
    double angle_potential_method;
    double init_theta;

    //// ширина робота
    double width;

};

struct _Message {
    char* message;
    int size;
    /**
     * 'm' - movement to a point,
     * 'i' - init initialize yourself among others
     * 's' - set own coordinates
     * 'n' - set second robot's coordinates
     * 'l' - set information about this and other robots
*/
    char type;

};

struct _Debug {
    /**
     * 'm' - movement to a point,
     * 'i' - init initialize yourself among others
     * 's' - set own coordinates
     * 'n' - set second robot's coordinates
     *
     * '0' - do nothing
     */
    char type;
    unsigned int timestamp;
    int at_target;
    int period;
};

struct _Mem_movement {

    int wait;
    double angle_start;
    double angle_target;
    bool top0;
    bool top1;
    bool top2;
    bool top3;
    bool top4;
    bool top5;

};

struct _Joint_movement {

    double x_shift;
    double y_shift;

    bool consider;

};

struct _Agent {

    int s_num;
    bool isLeader;
    double x, y;

};

typedef struct _Agent Agent;

typedef struct _Debug Debug;

typedef struct _Movement Movement;

typedef struct _Message Message;

typedef struct _Mem_movement Mem_movement;

typedef struct _Joint_movement Joint_movement;


struct _Robot {

    Movement movement;

    Mem_movement mem_movement;

    Joint_movement joint_movement;

    Agent* agents;
    int agents_num;
    int leader_id;
    bool is_manager;

    Message* messages;
    int message_num;

    /**
     * 'm' - movement to a point,
     * 'i' - init initialize yourself among others
     * 's' - set own coordinates
     * 'n' - set second robot's coordinates
     *
     * '0' - do nothing
     */
    char mode;

    Debug debug;

    int tact_num;

};

typedef struct _Robot Robot;

#endif //E_PUCK2_MAIN_PROCESSOR_DATA_MODELS_H
