/*
 * movement.h
 *
 *  Created on: Jan 28, 2019
 *      Author: smirnovvlad
 */
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <math.h>
//
//#include "ch.h"
//#include "chprintf.h"
//#include "hal.h"
//#include "shell.h"
//
//#include "aseba_vm/aseba_node.h"
//#include "aseba_vm/skel_user.h"
//#include "aseba_vm/aseba_can_interface.h"
//#include "aseba_vm/aseba_bridge.h"
//#include "audio/audio_thread.h"
//#include "audio/play_melody.h"
//#include "audio/play_sound_file.h"
//#include "audio/microphone.h"
//#include "camera/po8030.h"
//#include "epuck1x/Asercom.h"
//#include "epuck1x/Asercom2.h"
//#include "epuck1x/a_d/advance_ad_scan/e_acc.h"
//#include "epuck1x/motor_led/advance_one_timer/e_led.h"
//#include "epuck1x/utility/utility.h"
//#include "sensors/battery_level.h"
//#include "sensors/imu.h"
//#include "sensors/mpu9250.h"
//#include "sensors/proximity.h"

//#include "button.h"
//#include "cmd.h"
//#include "config_flash_storage.h"
//#include "exti.h"
//#include "fat.h"
//#include "i2c_bus.h"
//#include "ir_remote.h"
//#include "leds.h"
//#include <main.h>
//#include "memory_protection.h"
//#include "motors.h"
//#include "sdio.h"
//#include "selector.h"
//#include "spi_comm.h"
//#include "usbcfg.h"
//#include "communication.h"
//#include "uc_usage.h"

#ifndef SRC_MOVEMENT_MOVEMENT_H_
#define SRC_MOVEMENT_MOVEMENT_H_

#include "sensors/VL53L0X/VL53L0X.h"
#include "movement_controller.h"
#include "multiagent/model/data_models.h"

void init_movement(Robot* robot);

void run_movement(Robot* robot);

void set_goal_coordinates(Robot* robot, double x_new_target, double y_new_target);
void set_own_coordinates(Robot* robot, double x_new_target, double y_new_target);
void set_shift_coordinates(Robot *robot, double x, double y);

void calculate_coordiantes_to_robot (Robot* robot, double x_new_target, double y_new_target);


//
//void Update_goal_point_distance(double *Goal_point_distance, double X_pos_actual, double Y_pos_actual, double X_goal_point, double Y_goal_point);
//
//void Update_goal_point_angle(double *Goal_point_angle, double X_pos_actual, double Y_pos_actual, double X_goal_point, double Y_goal_point);
//
//void Update_angle_disagreement(double *Angle_disagreement, double Theta, double Goal_angle);
//
//void Update_odometer2(double *Left_encoder_old, double *Right_encoder_old, double *Theta, double *X_pos_actual, double *Y_pos_actual);
//
//void Update_goal_speeds_values(double *Linear_speed_goal, double *Angular_speed_goal, double Goal_point_distance, double Dec_radius,
//		double Stop_radius, double Linear_speed_max, double Angle_disagreement, double Angular_speed_max, double Dec_angle,
//		uint8_t Potential_method_switch, double Moving_speed_value_potential_method);
//
//void Calculate_speeds(double *Linear_speed_real, double *Angular_speed_real, double Linear_acc, double Angular_acc, double Linear_speed_goal,
//		double Angular_speed_goal);
//
//void Set_wheels_speeds(double Linear_speed_real, double Angular_speed_real, double h);
//
//void Filter_and_update_proximity_values(int *Prox_0_sens_values, int *Prox_1_sens_values, int *Prox_2_sens_values, int *Prox_5_sens_values,
//		int *Prox_6_sens_values, int *Prox_7_sens_values, int Size, int *Filtered_proximity_value);
//
//double Get_proximity_value_mm(int Data_from_sensor);
//
//void Update_obstacle_repulsion_vector(double Proximity_values_mm[], double Theta, double Sensor_angles[], double *Vector_angle, double *Vector_length);
//
//void Update_moving_vector_for_potential_method(double Obstacle_repulsion_vector_angle, double Obstacle_repulsion_vector_length, double Goal_point_angle,
//		double *Moving_speed_value_potential_method, double *Moving_angle_potential_method, double Linear_speed_max);
//
//void Update_odometer1(double *Left_encoder_old, double *Right_encoder_old, double *Theta, double *X_pos_actual,
//                      double *Y_pos_actual);


#endif /* SRC_MOVEMENT_MOVEMENT_H_ */
