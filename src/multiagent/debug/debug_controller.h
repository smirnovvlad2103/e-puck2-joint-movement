//
// Created by Смирнов Влад on 2019-01-29.
//

#ifndef E_PUCK2_MAIN_PROCESSOR_DEBUG_CONTROLLER_H
#define E_PUCK2_MAIN_PROCESSOR_DEBUG_CONTROLLER_H

#include "multiagent/model/data_models.h"


void init_debug(Robot* robot);

void run_debug(Robot* robot);


#endif //E_PUCK2_MAIN_PROCESSOR_DEBUG_CONTROLLER_H
