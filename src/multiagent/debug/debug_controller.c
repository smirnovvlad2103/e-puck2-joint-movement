//
// Created by Смирнов Влад on 2019-01-29.
//

#include "ch.h"

#include <stdlib.h>
#include <string.h>



#include "aseba_vm/aseba_node.h"
#include "aseba_vm/skel_user.h"
#include "aseba_vm/aseba_can_interface.h"
#include "aseba_vm/aseba_bridge.h"
#include "audio/audio_thread.h"
#include "audio/play_melody.h"
#include "audio/play_sound_file.h"
#include "audio/microphone.h"
#include "camera/po8030.h"
#include "epuck1x/Asercom.h"
#include "epuck1x/Asercom2.h"
#include "epuck1x/a_d/advance_ad_scan/e_acc.h"
#include "epuck1x/motor_led/advance_one_timer/e_led.h"
#include "epuck1x/utility/utility.h"
#include "sensors/battery_level.h"
#include "sensors/imu.h"
#include "sensors/mpu9250.h"
#include "sensors/proximity.h"
#include "sensors/VL53L0X/VL53L0X.h"
#include "button.h"
#include "cmd.h"
#include "config_flash_storage.h"
#include "exti.h"
#include "fat.h"
#include "i2c_bus.h"
#include "ir_remote.h"
#include "leds.h"
#include <main.h>
#include "memory_protection.h"
#include "memory.h"
#include "motors.h"
#include "sdio.h"
#include "selector.h"
#include "spi_comm.h"
#include "usbcfg.h"
#include "communication.h"
#include "uc_usage.h"


#include "debug_controller.h"
#include "multiagent/model/data_models.h"
#include "multiagent/movement/movement_controller.h"
#include "multiagent/bt/bt_controller.h"
#include "../model/data_models.h"
#include <uart/e_uart_char.h>
#include <../../ChibiOS/os/hal/lib/streams/chprintf.h>




void init_debug(Robot* robot) {

    Debug debug;
    debug.type = '0';
    debug.timestamp = chVTGetSystemTime();
    debug.period = 1000;
    debug.at_target = 1;

    robot->debug = debug;
    robot->mode = '0';
}

void run_debug(Robot* robot) {

    if (chVTGetSystemTime() > robot->debug.timestamp + robot->debug.period + (int) (robot->movement.y_real * 50.0) ) {

        if (SDU1.config->usbp->state == 4) {
            chprintf ((BaseSequentialStream *) &SDU1,
                      "-----------------run_debug_%u----------------\r\n", robot->debug.timestamp);

            chprintf ((BaseSequentialStream *) &SDU1,
                      "angle=%lf\r\n", robot->movement.disagreement_angle);

            chprintf ((BaseSequentialStream *) &SDU1,
                      "x=%lf_y=%lf_mode=%c_d_type=%c_lead=%d\r\n", robot->movement.x_real, robot->movement.y_real, robot->mode, robot->debug.type, robot->leader_id);
//            chprintf ((BaseSequentialStream *) &SDU1,
//                      "x=%lf_y=%lf\r\n", robot->movement.x_target, robot->movement.y_target);
        }

        Message message;

        int n = 0;

        switch (robot->debug.type) {
//            case 'm':
//
//                if (SDU1.config->usbp->state == 4) {
//                    chprintf ((BaseSequentialStream *) &SDU1,
//                              "------------------send_M------------------\r\n");
//                }
//
//                if (robot->movement.target_distance > robot->movement.stop_radius) {
//
//                    n = 25;
//
//                    message.type = 'd';
//                    message.message = malloc(sizeof(char) * n);
//                    message.message[0] = message.type;
//                    message.message[1] = '2';
//                    message.message[2] = '2';
//                    char ans[] = "target_distance";
//                    memcpy(message.message + 3, ans, strlen(ans) * sizeof(char));
//                    double_to_string(robot->movement.target_distance, message.message, 18, 25);
//                    message.size = 25;
//
//                    e_send_uart1_char (message.message, message.size);
//                    while(e_uart1_sending());
//
//                    free(message.message);
//
////                    add_message(robot, &message);
//
//
//
//                    robot->debug.timestamp = chVTGetSystemTime();
//                }
//
//                break;
            case 'm':
            case 'M':

                if (SDU1.config->usbp->state == 4) {
                    chprintf ((BaseSequentialStream *) &SDU1,
                              "------------------send_mM------------------\r\n");
                }
//                if (SDU1.config->usbp->state == 4) {
//                    chprintf ((BaseSequentialStream *) &SDU1,
//                              "--------x_shift=%lf, y_shift=%lf\r\n", robot->joint_movement.x_shift, robot->joint_movement.y_shift);
//                }


                if (robot->movement.target_distance > robot->movement.stop_radius || robot->debug.at_target == 1) {

                    if (robot->movement.target_distance <= robot->movement.stop_radius) {
                        robot->debug.at_target = 0;
                    } else {
                        robot->debug.at_target = 1;
                    }


                    n = 15;

                    message.type = 'd';
                    message.message = malloc(sizeof(char) * n);
                    message.message[0] = message.type;
                    message.message[1] = '1';
                    message.message[2] = '2';
//                    char ans[] = "target_distance";
//                    memcpy(message.message + 3, ans, strlen(ans) * sizeof(char));
                    double_to_string(robot->movement.x_real, message.message, 3, 7);
                    double_to_string(robot->movement.y_real, message.message, 7, 11);
                    double_to_string(robot->movement.theta, message.message, 11, 15);
                    message.size = n;

                    e_send_uart1_char (message.message, message.size);
                    while(e_uart1_sending());

//                    add_message(robot, &message);

                    free(message.message);

                    robot->debug.timestamp = chVTGetSystemTime();
                }

                break;

            case 'n':

                if (SDU1.config->usbp->state == 4) {
                    chprintf ((BaseSequentialStream *) &SDU1,
                              "------------------send_N------------------\r\n");
                }

                message.type = 'n';

                if (SDU1.config->usbp->state == 4) {
                    chprintf ((BaseSequentialStream *) &SDU1,
                              "------------------send_N_1------------------\r\n");
                }

                n = 10;
                message.message = malloc(sizeof(char) * n);
                message.message[0] = message.type;
                message.message[1] = '8';
                double_to_string(robot->movement.x_real, message.message, 2, 6);
                double_to_string(robot->movement.y_real, message.message, 6, 10);
                message.size = 10;

                if (SDU1.config->usbp->state == 4) {
                    chprintf ((BaseSequentialStream *) &SDU1,
                              "------------------send_N_2------------------\r\n");
                }

                e_send_uart1_char (message.message, message.size);
                while(e_uart1_sending());

                free(message.message);

//                add_message(robot, &message);

                robot->debug.timestamp = chVTGetSystemTime();

                if (SDU1.config->usbp->state == 4) {
                    chprintf ((BaseSequentialStream *) &SDU1,
                              "------------------send_N_3------------------\r\n");
                }

                break;

            case 'N':

                if (SDU1.config->usbp->state == 4) {
                    chprintf ((BaseSequentialStream *) &SDU1,
                              "------------------send_N------------------\r\n");
                }

                message.type = 'N';

                if (SDU1.config->usbp->state == 4) {
                    chprintf ((BaseSequentialStream *) &SDU1,
                              "------------------send_N_1------------------\r\n");
                }

                n = 14;
                message.message = malloc(sizeof(char) * n);
                message.message[0] = message.type;
                message.message[1] = '<';
                double_to_string(robot->movement.x_real, message.message, 2, 6);
                double_to_string(robot->movement.y_real, message.message, 6, 10);
                double_to_string(robot->movement.theta, message.message, 10, 14);
                message.size = 14;

                if (SDU1.config->usbp->state == 4) {
                    chprintf ((BaseSequentialStream *) &SDU1,
                              "------------------send_N_2------------------\r\n");
                }

                e_send_uart1_char (message.message, message.size);
                while(e_uart1_sending());

                free(message.message);

//                add_message(robot, &message);

                robot->debug.timestamp = chVTGetSystemTime();

                if (SDU1.config->usbp->state == 4) {
                    chprintf ((BaseSequentialStream *) &SDU1,
                              "------------------send_N_3------------------\r\n");
                }

                break;

            default:

                break;

        }
        robot->debug.timestamp = chVTGetSystemTime();

    }
}
